class MainTableViewController < UITableViewController

  def viewDidLoad
	  super

    self.title = "TrashPile"

    SVProgressHUD.showWithStatus "Picking up the Trashies", maskType:SVProgressHUDMaskTypeNone

    Dispatch::Queue.concurrent('data').async {
      metadata_str = File.read("#{App.resources_path}/data/METADATA.json") 
      @all_metadata = BW::JSON.parse(metadata_str) #.sort{|a,b| a['name'] <=> b['name']}
      keep_keys = %w{name item series team finish rarity imagefile}
      skip_keys = %w{id url_product url_image url_image_name imagepath}
      @all_metadata.each do |meta|
        skip_keys.each do |key|
          meta.delete(key)
        end
      end
      @metadata = @all_metadata
      self.view.reloadData
      SVProgressHUD.dismiss
    }

    @cellIdentifier = "trashieCell"
    uinib = UINib.nibWithNibName @cellIdentifier, bundle:nil
    tableView.registerNib uinib, forCellReuseIdentifier:@cellIdentifier
    tableView.rowHeight = 100

    tableView.setSeparatorStyle UITableViewCellSeparatorStyleNone

    swipeRecognizer = UISwipeGestureRecognizer.alloc.initWithTarget self, action:"onSwipeLeft:"
    swipeRecognizer.setDelegate self
    swipeRecognizer.setDirection UISwipeGestureRecognizerDirectionLeft
    tableView.addGestureRecognizer swipeRecognizer

    swipeRecognizer = UISwipeGestureRecognizer.alloc.initWithTarget self, action:"onSwipeRight:"
    swipeRecognizer.setDelegate self
    swipeRecognizer.setDirection UISwipeGestureRecognizerDirectionRight
    tableView.addGestureRecognizer swipeRecognizer

    search_bar = UISearchBar.alloc.initWithFrame [[0,0], [320,44]]
    search_bar.delegate = self
    view.addSubview search_bar
    view.tableHeaderView = search_bar
    @search_results = []
  end

  def search_for(text)
    NSLog "MainTableViewController#search_for #{text}"
    @metadata = []
    @all_metadata.each do |meta|
      # TODO: limit search to 'public' key fields
      @metadata << meta unless meta.values.map(&:to_s).map(&:downcase).grep(/#{text.downcase}/).empty?
    end
    navigationItem.title = "#{text} (#{@metadata.count})"
    view.reloadData
  end

  def searchBarSearchButtonClicked(search_bar)
    NSLog "MainTableViewController#searchBarSearchButtonClicked"
    @search_results.clear
    search_bar.resignFirstResponder
    navigationItem.title = search_bar.text
    search_for(search_bar.text)
    search_bar.text = ""
  end

  def searchBarCancelButtonClicked(search_bar) # not working
    NSLog "MainTableViewController#searchBarCancelButtonClicked"
    search_bar.resignFirstResponder
    navigationItem.title = "TrashPile"
  end

  def onSwipeLeft(recognizer)
    NSLog "MainTableViewController#onSwipeLeft"
    if (recognizer.state == UIGestureRecognizerStateEnded)
      swipeLocation = recognizer.locationInView(tableView)
      swipedIndexPath = tableView.indexPathForRowAtPoint(swipeLocation)
      NSLog "MainTableViewController#onSwipeLeft indexPath.row #{swipedIndexPath.row}"
      swipedCell = tableView.cellForRowAtIndexPath(swipedIndexPath)

      image = UIImage.imageNamed "crumpled_paper_yellow.png"
      image.stretchableImageWithLeftCapWidth 0.0, topCapHeight:5.0
      swipedCell.backgroundView = UIImageView.alloc.initWithImage image
    end
  end

  def onSwipeRight(recognizer)
    NSLog "MainTableViewController#onSwipeRight"
    if (recognizer.state == UIGestureRecognizerStateEnded)
      swipeLocation = recognizer.locationInView(tableView)
      swipedIndexPath = tableView.indexPathForRowAtPoint(swipeLocation)
      NSLog "MainTableViewController#onSwipeRight indexPath.row #{swipedIndexPath.row}"
      swipedCell = tableView.cellForRowAtIndexPath(swipedIndexPath)

      image = UIImage.imageNamed "crumpled_paper.png"
      image.stretchableImageWithLeftCapWidth 0.0, topCapHeight:5.0
      swipedCell.backgroundView = UIImageView.alloc.initWithImage image
    end
  end

	## Table view data source

  def numberOfSectionsInTableView(tableView)
    1
  end

  def tableView(tableView, numberOfRowsInSection:section)
    @metadata ? @metadata.length : 0
  end

  def tableView(tableView, cellForRowAtIndexPath:indexPath)
    trashie = @metadata[indexPath.row]

    cell = tableView.dequeueReusableCellWithIdentifier(@cellIdentifier) 

    cellImage = cell.viewWithTag 1
    cellItemName= cell.viewWithTag 2
    cellSeriesTeam = cell.viewWithTag 3
    cellFinishRarity = cell.viewWithTag 4

    cellImage.image = UIImage.imageNamed("data/#{trashie['imagefile'].gsub(/.png/,'')}")
    cellItemName.text = "##{trashie['item']} #{trashie['name']}"
    cellSeriesTeam.text = "#{trashie['series']} / #{trashie['team']}"
    cellFinishRarity.text = "#{trashie['finish']} / #{trashie['rarity']}"

    image = UIImage.imageNamed "crumpled_paper.png"
    image.stretchableImageWithLeftCapWidth 0.0, topCapHeight:5.0
    cell.backgroundView = UIImageView.alloc.initWithImage image  

    cell
  end

	## Table view delegate

  def tableView(tableView, didSelectRowAtIndexPath:indexPath)
    # Navigation logic may go here. Create and push another view controller.
    # detailViewController = DetailViewController.alloc.initWithNibName("Nib name", bundle:nil)
    # Pass the selected object to the new view controller.
    # self.navigationController.pushViewController(detailViewController, animated:true)
  end

end
