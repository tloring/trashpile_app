class AppDelegate
  def application(application, didFinishLaunchingWithOptions:launchOptions)
    @window = UIWindow.alloc.initWithFrame UIScreen.mainScreen.bounds
    mtvc = MainTableViewController.alloc.init
    @window.rootViewController = UINavigationController.alloc.initWithRootViewController mtvc
    @window.makeKeyAndVisible
    true
  end
end
